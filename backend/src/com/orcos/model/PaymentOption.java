package com.orcos.model;

public class PaymentOption {
	
	private Address address;
	private User user;
	private PaymentMethod paymentMethod;
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public void addNewCard() {
		
	}
	
	public void removeCard() {
		
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaymentOption [address=").append(address).append(", user=").append(user)
				.append(", paymentMethod=").append(paymentMethod).append("]");
		return builder.toString();
	}
	
	
	
	

}
