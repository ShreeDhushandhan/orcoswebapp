package com.orcos.model;

public class Address {
	
	private String street;
	private String city;
	private long postalCode;
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public long getPostalCode() {
		return postalCode;
	}
	
	public void setPostalCode(long postalCode) {
		this.postalCode = postalCode;
	}
	
	public void addAddress() {
		
	}
	
	public void editAddress() {
		
	}
	
	public void deleteAddress() {
		
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Address [street=").append(street).append(", city=").append(city).append(", postalCode=")
				.append(postalCode).append("]");
		return builder.toString();
	}
	
	
	
	

}
