package com.orcos.model;

import java.util.ArrayList;

public class Order {
	
	private ArrayList itemList;
	private ArrayList buyAgain;
	private ArrayList cancelOrders;
	
	public ArrayList getItemList() {
		return itemList;
	}
	
	public void setItemList(ArrayList itemList) {
		this.itemList = itemList;
	}
	
	public ArrayList getBuyAgain() {
		return buyAgain;
	}
	
	public void setBuyAgain(ArrayList buyAgain) {
		this.buyAgain = buyAgain;
	}
	
	public ArrayList getCancelOrders() {
		return cancelOrders;
	}
	
	public void setCancelOrders(ArrayList cancelOrders) {
		this.cancelOrders = cancelOrders;
	}
	
	public void addToCart() {
		
	}
	

}
