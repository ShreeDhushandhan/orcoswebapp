package com.orcos.model;

public class PaymentMethod {
	
	private String nameOnCard;
	private long cardNumber;
	private long expiryMonth;
	private long expiryYear;
	
	public String getNameOnCard() {
		return nameOnCard;
	}
	
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	
	public long getCardNumber() {
		return cardNumber;
	}
	
	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}
	
	public long getExpiryMonth() {
		return expiryMonth;
	}
	
	public void setExpiryMonth(long expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	
	public long getExpiryYear() {
		return expiryYear;
	}
	
	public void setExpiryYear(long expiryYear) {
		this.expiryYear = expiryYear;
	}
	
	public void addYouCard() {
		 
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaymentOptions [nameOnCard=").append(nameOnCard).append(", cardNumber=").append(cardNumber)
				.append(", expiryMonth=").append(expiryMonth).append(", expiryYear=").append(expiryYear).append("]");
		return builder.toString();
	}
	
	
	
	

}
