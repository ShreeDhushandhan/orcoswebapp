package com.orcos.model;

public class Account {
	
	private Order order;
	private User user;
	private Address address;
	private PaymentOption paymentOption;
	
	public Order getOrder() {
		return order;
	}
	
	public void setOrder(Order order) {
		this.order = order;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public PaymentOption getPaymentOptions() {
		return paymentOption;
	}
	
	public void setPaymentOptions(PaymentOption paymentOptions) {
		this.paymentOption = paymentOptions;
	}
	
	public void goToOrders() {
		
	}
	
	public void goToAddress() {
		
	}
	
	public void goToPayment() {
		
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Account [user=").append(user).append("]");
		return builder.toString();
	}
	

}
