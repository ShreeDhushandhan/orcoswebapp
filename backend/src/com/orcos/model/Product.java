package com.orcos.model;

public class Product {
	
	private String name;
	private long price;
	private long availableCount;
	private String description;
	private String deliveryTime;
	private String image;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public long getPrice() {
		return price;
	}
	
	public void setPrice(long price) {
		this.price = price;
	}
	
	public long getAvailableCount() {
		return availableCount;
	}
	
	public void setAvailableCount(long availableCount) {
		this.availableCount = availableCount;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDeliveryTime() {
		return deliveryTime;
	}
	
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	public void buyNow() {
		
	}
	
	public void addToCart() {
		
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [name=");
		builder.append(name);
		builder.append(", price=");
		builder.append(price);
		builder.append(", availableCount=");
		builder.append(availableCount);
		builder.append(", description=");
		builder.append(description);
		builder.append(", deliveryTime=");
		builder.append(deliveryTime);
		builder.append(", image=");
		builder.append(image);
		builder.append("]");
		return builder.toString();
	}
	
	
	

}
