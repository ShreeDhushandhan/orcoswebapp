package com.orcos.model;

import java.util.ArrayList;

public class Payment {
	
	private long amount;
	private long itemCount;
	private ArrayList items;
	private String modeOfPayment;
	
	public Payment() {
		
	}
	
	public Payment(long amount, long itemCount, ArrayList items, String modeOfPayment) {
		super();
		this.amount = amount;
		this.itemCount = itemCount;
		this.items = items;
		this.modeOfPayment = modeOfPayment;
	}

	public long getAmount() {
		return amount;
	}
	
	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	public long getItemCount() {
		return itemCount;
	}
	
	public void setItemCount(long itemCount) {
		this.itemCount = itemCount;
	}
	
	public ArrayList getItems() {
		return items;
	}
	
	public void setItems(ArrayList items) {
		this.items = items;
	}
	
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	
	public void pay() {
		
	}
	
	public void cancel() {
		
	}

	@Override
	public String toString() {
		
		return new StringBuilder("Payment [amount=")
				.append(amount)
				.append(", itemCount=")
				.append(itemCount)
				.append(", items=")
				.append(items)
				.append(", modeOfPayment=")
				.append(modeOfPayment)
				.append("]")
				.toString();
	}

}
