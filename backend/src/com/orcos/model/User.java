package com.orcos.model;

public class User {
	
	String firstName;
	String lastName;
	String email;
	String password;
	long phoneNumber;
	
	public User() {
		
	}
	
	public User(String firstName, String lastName, String email, String password, long phoneNumber) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phoneNumber = phoneNumber;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public long getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public void login() {
		
	}
	
	public void editProfile() {
		
	}
	
	public void logout() {
		
	}
	
	@Override
	public String toString() {
		
		return new StringBuilder("User [firstName=")
				.append(firstName)
				.append(", lastName=")
				.append(lastName)
				.append(", email=")
				.append(email)
				.append(", password=")
				.append(password)
				.append(", phoneNumber=")
				.append(phoneNumber)
				.append("]")
				.toString();
	}
	

}
