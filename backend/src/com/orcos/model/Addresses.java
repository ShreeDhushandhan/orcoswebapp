package com.orcos.model;

public class Addresses {
	
	private String name;
	private String address;
	private long phoneNumber;
	private boolean defaultAddress;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public long getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public boolean isDefaultAddress() {
		return defaultAddress;
	}
	
	public void setDefaultAddress(boolean defaultAddress) {
		this.defaultAddress = defaultAddress;
	}
	
	public void addAddress() {
		
	}
	
	public void editAddress() {
		
	}
	
	public void removeAddress() {
		
	}
	
	public void setDefault() {
		
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Addresses [name=").append(name).append(", address=").append(address).append(", phoneNumber=")
				.append(phoneNumber).append(", defaultAddress=").append(defaultAddress).append("]");
		return builder.toString();
	}
	
	
	

}
