package com.orcos.model;

import java.util.ArrayList;

public class Cart {
	
	private ArrayList itemList;
	private long quantity;
	private long subTotal;
	
	public ArrayList getItemList() {
		return itemList;
	}
	
	public void setItemList(ArrayList itemList) {
		this.itemList = itemList;
	}
	
	public long getQuantity() {
		return quantity;
	}
	
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	
	public long getSubTotal() {
		return subTotal;
	}
	
	public void setSubTotal(long subTotal) {
		this.subTotal = subTotal;
	}
	
	public void proceedToBuy() {
		
	}
	
	public void deleteItem() {
		
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Cart [quantity=").append(quantity).append(", subTotal=").append(subTotal).append("]");
		return builder.toString();
	}
	
	
	
	

}
